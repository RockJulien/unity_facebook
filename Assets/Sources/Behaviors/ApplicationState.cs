﻿using UnityEngine;
using Views;

namespace Behaviors
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ApplicationState : MonoBehaviour
    {
        [SerializeField]
        private HeaderView _header = null;

        private void Start()
        {
            if ( _header != null )
            {
                _header.SetHeader( false, false, "Choose your account", "Either log-in with Facebook or Google" );
            }
        }
    }
}
