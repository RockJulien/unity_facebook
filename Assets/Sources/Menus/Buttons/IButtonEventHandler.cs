﻿using Enums;
using UnityEngine;

namespace Menu.Buttons
{
    /// <summary>
    /// Definition of the <see cref="IButtonEventHandler"/> interface.
    /// </summary>
    public interface IButtonEventHandler
    {
        void OnStateChanged(GameObject sender, ButtonStateEnum state);

        void OnClick(GameObject sender);

        void OnPressed(GameObject sender);

        void OnReleased(GameObject sender);

        void OnDoubleClick(GameObject sender);

        void OnEnter(GameObject sender);

        void OnExit(GameObject sender);
    }
}
