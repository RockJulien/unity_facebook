﻿using System;
using UnityEngine;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    internal class ButtonState : IButtonState
    {
        private Sprite _image = null;
        private Color  _color = Color.white;
        private string _title = string.Empty;

        public event Action<IButtonState> callbacks = null;

        public Sprite image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public Color color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }

        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }
        
        public ButtonState(Sprite image, Color color)
        {
            _image = image;
            _color = color;
        }

        public void NotifyListeners()
        {
            if ( callbacks != null )
            {
                callbacks( this );
            }
        }

        public void Clear()
        {
            if ( callbacks != null )
            {
                Delegate[] delegates = callbacks.GetInvocationList();
                for (int curr = 0; curr < delegates.Length; curr++)
                {
                    callbacks -= (Action<IButtonState>)delegates[ curr ];
                }
            }
        }

        public void Dispose()
        {
            Clear();
        }
    }
}
