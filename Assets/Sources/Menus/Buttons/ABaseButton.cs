﻿using Enums;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class ABaseButton : MonoBehaviour, IMoveHandler, IEventSystemHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
    {
        private ButtonStateEnum _currentState = ButtonStateEnum.NONE;

        private Dictionary<ButtonStateEnum, IButtonState> _states;
        private bool _hasClicked = false;

        [SerializeField]
        private bool    _isInteractable = true;

        [SerializeField]
        private Image   _targetGraphic  = null;

        public event Action<ABaseButton> onInitialized = null;
        public event Action<ABaseButton> onClick       = null;
        public event Action<ABaseButton> onPressed     = null;
        public event Action<ABaseButton> onReleased    = null;
        public event Action<ABaseButton> onDoubleClick = null;

        public event Action<ABaseButton> onSelected    = null;
        public event Action<ABaseButton> onDeselected  = null;

        public event Action<ABaseButton> onEnter       = null;
        public event Action<ABaseButton> onExit        = null;

        public IButtonEventHandler eventHandler = null;

        private void Start()
        {
            _states = new Dictionary<ButtonStateEnum, IButtonState>();
            
            Initialize();

            if ( onInitialized != null )
            {
                onInitialized( this );
            }
        }

        private void OnDestroy()
        {
            ClearStates();

            OnCustomDestroy();
        }

        private void ClearStates()
        {
            if ( _states != null )
            {
                foreach ( IButtonState state in _states.Values )
                {
                    state.Dispose();
                }
            }
        }
        
        public void AddState(ButtonStateEnum state, Sprite _image, Color _color)
        {
            IButtonState buttonState = new ButtonState( _image, _color );

            _states[ state ] = buttonState;

            OnStateAdded( state, buttonState );
        }

        protected virtual void OnStateAdded(ButtonStateEnum state, IButtonState buttonState)
        {

        }

        public void SetState(ButtonStateEnum state, ButtonStateContext context)
        {
            IButtonState buttonState = null;
            if ( _states.TryGetValue( state, out buttonState ) )
            {
                ButtonState stateButton = buttonState as ButtonState;
                if ( stateButton != null )
                {
                    stateButton.callbacks += context.callback;

                    if ( context.overrideImage != null )
                    {
                        stateButton.image = context.overrideImage;
                    }

                    stateButton.title = context.title;
                }

                OnCustomSetState( state, buttonState );
            }
        }
        
        protected virtual void OnCustomSetState(ButtonStateEnum state, IButtonState button)
        {

        }

        public void ChangeState(ButtonStateEnum state)
        {
            IButtonState buttonState = null;
            if ( _states.TryGetValue( state, out buttonState ) )
            {
                _currentState = state;

                if ( _targetGraphic != null )
                {
                    _targetGraphic.sprite = buttonState.image;
                    _targetGraphic.color  = buttonState.color;
                }

                NotifyStateChanged();

                OnStateChanged( _currentState );
            }
        }

        protected virtual void OnStateChanged(ButtonStateEnum state)
        {

        }

        private void NotifyStateChanged()
        {
            if ( eventHandler != null )
            {
                eventHandler.OnStateChanged( gameObject, _currentState );
            }
        }

        private void OnClick()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( onClick != null )
            {
                onClick( this );
            }

            if ( eventHandler != null )
            {
                eventHandler.OnClick( gameObject );
            }

            IButtonState buttonState = null;
            if ( _states.TryGetValue( _currentState, out buttonState ) )
            {
                buttonState.NotifyListeners();
            }

            OnCustomClick();
        }

        private void OnPressed()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( eventHandler != null )
            {
                eventHandler.OnPressed( gameObject );
            }

            if ( onPressed != null )
            {
                onPressed( this );
            }
        }

        private void OnReleased()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( eventHandler != null )
            {
                eventHandler.OnReleased( gameObject );
            }

            if ( onReleased != null )
            {
                onReleased( this );
            }
        }

        private void OnDoubleClick()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( eventHandler != null )
            {
                eventHandler.OnDoubleClick( gameObject );
            }

            if ( onDoubleClick != null )
            {
                onDoubleClick( this );
            }
        }

        private void OnEnter()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( eventHandler != null )
            {
                eventHandler.OnEnter( gameObject );
            }

            if ( onEnter != null )
            {
                onEnter( this );
            }
        }

        private void OnExit()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( eventHandler != null )
            {
                eventHandler.OnExit( gameObject );
            }

            if ( onExit != null )
            {
                onExit( this );
            }
        }

        private void OnSelect()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( onSelected != null )
            {
                onSelected( this );
            }
        }

        private void OnDeselect()
        {
            if ( _isInteractable == false )
            {
                return;
            }

            if ( onDeselected != null )
            {
                onDeselected( this );
            }
        }

        protected virtual void OnCustomClick()
        {

        }

        protected virtual void Initialize()
        {

        }

        protected virtual void OnCustomDestroy()
        {

        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            _hasClicked = true;

            OnPressed();
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if ( eventData.dragging )
            {
                return;
            }

            if ( _hasClicked )
            {
                if ( eventData.clickCount == 1 )
                {
                    OnReleased();
                }
                else if ( eventData.clickCount > 1 )
                {
                    OnDoubleClick();
                }

                OnClick();

                _hasClicked = false;
            }
        }

        void IMoveHandler.OnMove(AxisEventData eventData)
        {
            
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            OnEnter();
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            OnExit();
        }

        void ISelectHandler.OnSelect(BaseEventData eventData)
        {
            OnSelect();
        }

        void IDeselectHandler.OnDeselect(BaseEventData eventData)
        {
            OnDeselect();
        }

        public void Select()
        {
            if ( EventSystem.current )
            {
                EventSystem.current.SetSelectedGameObject( gameObject );
            }
        }

        public void Deselect()
        {
            if ( EventSystem.current )
            {
                EventSystem.current.SetSelectedGameObject( null );
            }
        }
    }
}
