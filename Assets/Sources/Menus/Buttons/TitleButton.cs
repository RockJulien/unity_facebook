﻿using UnityEngine;
using UnityEngine.UI;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TitleButton : StandardButton
    {
        [Header("Title")]
        [SerializeField]
        private Text _title = null;

        /// <summary>
        /// Refreshes the title.
        /// </summary>
        /// <param name="title"></param>
        public void RefreshTitle(string title)
        {
            if ( _title != null )
            {
                _title.text = title;
            }
        }
    }
}
