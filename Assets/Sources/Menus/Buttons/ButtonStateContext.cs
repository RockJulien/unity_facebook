﻿using System;
using UnityEngine;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ButtonStateContext
    {
        private Action<IButtonState> _callback = null;

        private string _title = string.Empty;

        private Sprite _overrideImage = null;

        public Action<IButtonState> callback
        {
            get
            {
                return _callback;
            }
        }

        public string title
        {
            get
            {
                return _title;
            }
        }

        public Sprite overrideImage
        {
            get
            {
                return _overrideImage;
            }
        }

        public ButtonStateContext(Action<IButtonState> callback, string title, Sprite overrideImage)
        {
            _callback = callback;
            _title = title;
            _overrideImage = overrideImage;
        }
    }
}
