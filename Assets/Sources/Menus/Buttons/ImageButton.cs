﻿using UnityEngine;
using UnityEngine.UI;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ImageButton : StandardButton
    {
        [Header("Icon")]
        [SerializeField]
        private Image _image = null;

        public void RefreshIcon(Sprite icon)
        {
            if ( _image != null )
            {
                _image.sprite = icon;
            }
        }
    }
}
