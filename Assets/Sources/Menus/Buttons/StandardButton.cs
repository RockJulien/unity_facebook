﻿using Enums;
using UnityEngine;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class StandardButton : ABaseButton
    {
        [Header("Backgrounds")]
        [SerializeField] private Sprite _background          = null;
        [SerializeField] private Sprite _highlightBackground = null;
        [SerializeField] private Sprite _lockBackground      = null;
        [SerializeField] private Sprite _disableBackground   = null;

        protected override void Initialize()
        {
            base.Initialize();

            this.AddState( ButtonStateEnum.NORMAL,    _background,          Color.white );
            this.AddState( ButtonStateEnum.HIGHLIGHT, _highlightBackground, Color.white );
            this.AddState( ButtonStateEnum.LOCKED,    _lockBackground,      Color.white );
            this.AddState( ButtonStateEnum.DISABLED,  _disableBackground,   Color.white );
        }
    }
}
