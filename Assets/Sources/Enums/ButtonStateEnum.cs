﻿namespace Enums
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public enum ButtonStateEnum
    {
        NONE = 0,

        NORMAL = 10,

        HIGHLIGHT = 20,

        CALL_TO_ACTION = 30,

        LOCKED = 40,

        NOT_ENOUGH = 50,

        DISABLED = 60,
    }
}
