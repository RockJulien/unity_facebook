﻿using Enums;
using Menu.Buttons;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class PlayerLoginView : MonoBehaviour
    {
        [SerializeField]
        private StandardButton _facebookButton = null;

        [SerializeField]
        private StandardButton _googleButton   = null;

        private void Start()
        {
            if ( _facebookButton != null )
            {
                _facebookButton.onInitialized += OnFacebookButtonInitialized;
            }

            if ( _googleButton != null )
            {
                _googleButton.onInitialized += OnGoogleButtonInitialized;
            }
        }
        
        private void OnFacebookButtonInitialized(ABaseButton button)
        {
            _facebookButton.onInitialized -= OnFacebookButtonInitialized;
            _facebookButton.SetState( ButtonStateEnum.HIGHLIGHT, new ButtonStateContext( OnFacebookClick, "FACEBOOK SIGN-IN", null ) );
            _facebookButton.ChangeState( ButtonStateEnum.HIGHLIGHT );
        }

        private void OnGoogleButtonInitialized(ABaseButton button)
        {
            _googleButton.onInitialized -= OnGoogleButtonInitialized;
            _googleButton.SetState( ButtonStateEnum.HIGHLIGHT, new ButtonStateContext( OnGoogleClick, "GOOGLE SIGN-IN", null ) );
            _googleButton.ChangeState( ButtonStateEnum.HIGHLIGHT );
        }

        private void OnFacebookClick(IButtonState state)
        {
            Debug.Log( "Clicked on facebook" );
        }

        private void OnGoogleClick(IButtonState state)
        {
            Debug.Log( "Clicked on google" );
        }
    }
}
