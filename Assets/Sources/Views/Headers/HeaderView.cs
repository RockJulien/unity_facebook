﻿using Menu.Buttons;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HeaderView : MonoBehaviour
    {
        [SerializeField]
        private ImageButton _backButton = null;

        [SerializeField]
        private HeaderInfoView _info = null;

        [SerializeField]
        private ImageButton _homeButton = null;

        public void SetHeader(bool canGoBack = false, bool canGoHome = false, string title = null, string subtitle = null)
        {
            if ( _backButton != null )
            {
                _backButton.gameObject.SetActive( canGoBack );
            }

            if ( _info != null )
            {
                _info.SetInfo( title, subtitle );
            }

            if ( _homeButton != null )
            {
                _homeButton.gameObject.SetActive( canGoHome );
            }
        }
    }
}
