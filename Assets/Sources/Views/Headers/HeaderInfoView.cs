﻿using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HeaderInfoView : MonoBehaviour
    {
        [SerializeField]
        private Text _title = null;

        [SerializeField]
        private Text _subtitle = null;

        public void SetInfo(string title = null, string subtitle = null)
        {
            if ( _title != null )
            {
                if ( string.IsNullOrEmpty( title ) )
                {
                    _title.gameObject.SetActive( false );
                }
                else
                {
                    _title.gameObject.SetActive( true );

                    _title.text = title;
                }
            }

            if ( _subtitle != null )
            {
                if ( string.IsNullOrEmpty( subtitle ) )
                {
                    _subtitle.gameObject.SetActive( false );
                }
                else
                {
                    _subtitle.gameObject.SetActive( true );

                    _subtitle.text = subtitle;
                }
            }
        }
    }
}
